(defproject jobqueueapi "0.1.0-SNAPSHOT"
  :description "My take from the Nubank code challenge (2nd part)"
  :url "http://xtend.com.br:6000/"
  :min-lein-version "2.0.0"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [cheshire "5.4.0"]
                 [metosin/compojure-api "2.0.0-alpha18"]
                 [metosin/ring-http-response "0.8.2"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-json "0.4.0"]
                 [org.clojure/tools.cli "0.3.5"]]
  :main ^:skip-aot jobqueueapi.endpoints
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler jobqueueapi.handler/app}
  :target-path "target/%s"
  :manifest {"Class-Path" "lib/clojure-1.2.0.jar"}
  :profiles {:uberjar {:aot :all}
             :dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring/ring-mock "0.3.2"]]}})
