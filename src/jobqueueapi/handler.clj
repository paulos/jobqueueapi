(ns jobqueueapi.handler
  (:require [jobqueueapi.endpoints :as endpoints]
            [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [schema.core :as s])
  (:import (java.awt JobAttributes)))

(s/defschema Agent
  {:id s/Str
   :name s/Str
   :primary_skillset [s/Str]
   :secondary_skillset [s/Str]})

(s/defschema Job
  {:id s/Str
   :type s/Str
   :urgent s/Bool})

(s/defschema JobRequest
  {:agent_id s/Str})

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Jobqueues API"
                    :description "Paulo's take on the Nubank Code Challenge Part II: the revenge of the API"}
             :tags [{:name "api" :description "Job Queue API"}]}}}

    (context "/api" []
      :tags ["api"]

      (GET "/agents/" []
        :summary "list agents in the database"
        (ok {:result (endpoints/list-agents)}))
      (POST "/agents/" []
        :return Agent
        :body [agent Agent]
        :summary "creates a new agent in the database"
        (ok (endpoints/add-agent agent)))
      (GET "/agents/:id/" [id]
        :summary "get agent details (including job assignment stats)"
        (let [result (endpoints/get-agent id)]
          (if result
            (ok {:result result})
            (not-found {:reason (str id " does not compute! https://www.youtube.com/watch?v=ZBAijg5Betw")}))))
      (GET "/jobs/" []
        :summary "A breakdown of the job queue, considering all completed, being done and waiting jobs"
        (ok {:result (endpoints/list-pending-jobs)}))
      (POST "/jobs/" []
        :return Job
        :body [job Job]
        :summary "add a new job to the database"
        (ok (endpoints/add-job job)))
      (POST "/job-requests/" []
        :body [jobrequest JobRequest]
        :summary "creates a job request - returns 404 if there is no suitable job"
        (let [result (endpoints/create-job-request jobrequest)]
          (cond
            (nil? (result :agent_id)) (not-found {:reason "No such agent found"})
            (nil? (result :job_id)) (not-found {:reason "No suitable job at this moment"})
            :else (ok result))))
      )))

