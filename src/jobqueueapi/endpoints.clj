(ns jobqueueapi.endpoints
  (:use clojure.walk)
  (:require [cheshire.core :refer :all])
  (:gen-class))

(def agents (atom {}))
(def jobs (atom []))

(defn by-id [collection]
  "Return a map of {:id value} from a collection of maps containing an id key"
  (zipmap (map #(% :id) collection) collection))

(defn sort-by-urgent [collection]
  (flatten [(for [item collection :when (item :urgent)] item)
            (for [item collection :when (not (item :urgent))] item)]))

(defn get-stats [agent_id]
  "Counts how many jobs assigned for each skill"
  (let [agent (@agents agent_id)]
    (into (hash-map)
        (for [skill (flatten [(agent :primary_skillset) (agent :secondary_skillset)])]
          [skill (apply + (for [job @jobs :when (and (= (job :agent) agent_id) (= (job :type) skill))] 1))]))))

(defn get-agent [id]
  "Returns nil if no agent found otherwise returns agent"
  (let [agent (@agents id)]
    (and agent
      (assoc agent :stats (get-stats id)))))

(defn add-agent [agent]
  (let [id (agent :id)]
    (swap! agents assoc id agent)
    (@agents id)))

(defn list-agents []
  @agents)

(defn list-pending-jobs []
  (remove :agent @jobs))

(defn get-job [id]
  ((by-id @jobs) id))

(defn add-job [job]
  "TODO: probably should forbid a duplicate job_id"
  (swap! jobs conj job)
  (get-job (job :id)))

(defn assign-job [job_id agent_id]
  (swap! jobs (fn [joblist] (vec
                           (for [job joblist]
                             (if (= job_id (job :id))
                               (assoc job :agent agent_id)
                               (if (= agent_id (job :agent)) (assoc job :done true) job))))))
  {:job_id job_id :agent_id agent_id})

(defn best-match [agent_id]
  "Returns matching {:agent_id value :job_id value} where value is nil when no record found"
  (let [pending (sort-by-urgent (list-pending-jobs))
        skillsets [:primary_skillset :secondary_skillset]
        agent (@agents agent_id)
        job_id (and agent
                 (first (flatten
                          (for [skillset skillsets]
                            (for [job pending
                                  :let [type (job :type)]
                                  :when (some #{type} (agent skillset))]
                              (job :id))))))]
    (if job_id
      (assign-job job_id agent_id)
      {:agent_id (and agent agent_id) :job_id nil})))

(defn create-job-request [jobrequest]
  (let [agent_id (jobrequest :agent_id)]
    (best-match agent_id)))

