# jobqueueapi

This is the second part of the code challenge
from Nubank's recruiting process.

## Installation

clone from git@bitbucket.org:paulos/jobqueueapi.git.

Access granted to https://bitbucket.org/rodrigoflores/ like in the
 first part, please ask him if other user needs access (he is a repo admin).

## Usage

This should start the API server at http://localhost:3000

    $ lein ring server
    
There is a Swagger documentation page on the root URL so I'm not documenting
 the endpoints here. Have fun.

## Database

<del>App is using SQLite in-memory database. Should be trivial to 
use a permanent database instead.</del>

At my first take on this second part of the take-home code challenge
I've used an SQLite database but looks like I should not delegate logic to the 
database - the test objective is to see if the candidate can use "functional" 
constructs. Probably I use `for` a lot more than a seasoned clojure developers
but it is because they look like Python's generator expressions.

Anyway I got rid of the database code and did everything using native
clojure collections.


### Bugs

1. Concurrency: the reason why I first thought about using a database is 
because of the ACID guaranties, not because I can't do filter/map/reduce - 
I have no clue about how safe a shared collection is regarding concurrency 
in Clojure.

1. Endpoints possibly are not returning proper HTTP codes (404 for 
missing records, 201 Created, etc).

1. Several situations that should be tested are not. 

These are not too hard to fix but I assume this is OK for a toy 
project like this.

## License

Copyright © 2018 Paulo Scardine

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
