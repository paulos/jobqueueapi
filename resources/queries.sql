-- :name create-agent-table
-- :command :execute
-- :result :raw
CREATE TABLE agents (
  id TEXT PRIMARY KEY,
  name TEXT
);

-- :name create-skills-table
-- :command :execute
-- :result :raw
CREATE TABLE skills (
  agent_id TEXT,
  name TEXT,
  type INTEGER,  -- 0: primary, 1: secondary
  FOREIGN KEY (agent_id) REFERENCES agents(id)
);

-- :name create-jobs-table
-- :command :execute
-- :result :raw
CREATE TABLE jobs (
  seq INTEGER PRIMARY KEY,
  id TEXT,
  type TEXT,
  urgent INT  -- 0: normal, 1: urgent
);

-- :name create-assignments-table
-- :command :execute
-- :result :raw
CREATE TABLE assignments (
  agent_id TEXT,
  job_id TEXT,
  done INTEGER DEFAULT 0,  -- 0: doing, 1: done
  FOREIGN KEY (agent_id) REFERENCES agents(id),
  FOREIGN KEY (job_id) REFERENCES jobs(id)
);
CREATE UNIQUE INDEX unique_assignment ON assignments(agent_id, job_id);

-- :name insert-agent :! :n
INSERT INTO agents (id, name)
    VALUES (:id, :name);

-- :name insert-skill :! :n
INSERT INTO skills (agent_id, name, type)
    VALUES (:agent_id, :name, :type);

-- :name insert-job :! :n
INSERT INTO jobs (id, type, urgent)
    VALUES (:id, :type, :urgent);

-- :name assignments-done :! :n
UPDATE assignments
    SET done = 1
    WHERE agent_id = :agent_id;

-- :name insert-assignment :<! :raw
INSERT INTO assignments (agent_id, job_id)
    VALUES (agent_id, job_id);

-- :name get-best-match :! :n
INSERT INTO assignments (agent_id, job_id)
  SELECT agents.id, jobs.id
    FROM agents, skills, jobs
    WHERE agents.id = skills.agent_id
      AND jobs.type = skills.name
      AND agents.id = :agent_id
      AND NOT EXISTS(SELECT * FROM assignments WHERE job_id=jobs.id)
    ORDER BY jobs.urgent DESC, jobs.seq, skills.type
    LIMIT 1;

-- :name get-last-assignment :? :1
SELECT job_id FROM assignments WHERE done=0 AND agent_id=:agent_id;

-- :name get-agent-list :? :*
SELECT agents.* FROM agents;

-- :name get-skills :? :*
SELECT * FROM skills WHERE agent_id = :agent_id;

-- :name get-skill-list :? :*
SELECT * FROM skills;

-- :name get-agent-details :? :1
SELECT * FROM agents WHERE id = :id;

-- :name get-job-list :? :*
SELECT jobs.id, jobs.type, jobs.urgent, assignments.done, assignments.agent_id
  FROM jobs
       LEFT JOIN assignments ON jobs.id = assignments.job_id
  ORDER BY urgent DESC, seq;

-- :name get-job-details :? :1
SELECT id, type, urgent FROM jobs WHERE id = :id;

-- :name best-match :? :1
SELECT jobs.id AS job_id, skills.agent_id
  FROM jobs
    JOIN skills ON jobs.type = skills.name
    LEFT JOIN assignments ON assignments.job_id = jobs.id
  WHERE assignments.job_id IS NULL
    AND skills.agent_id = :agent_id
  ORDER BY jobs.urgent DESC, jobs.seq, skills.type
  LIMIT 1;

-- :name get-assignments :? :*
SELECT * FROM assignments;

-- :name agent-stats :? :*
SELECT skills.name AS skill, COUNT(jobs.id) AS total
  FROM skills
    LEFT JOIN jobs ON jobs.type=skills.name
    LEFT JOIN assignments ON jobs.id=assignments.job_id
  WHERE skills.agent_id=:agent_id
  GROUP BY skills.name;