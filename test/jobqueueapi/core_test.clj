(ns jobqueueapi.core-test
  (:require [clojure.test :refer :all]
            [cheshire.core :as cheshire]
            [jobqueueapi.handler :refer :all]
            [jobqueueapi.endpoints :refer :all]
            [ring.mock.request :as mock]))

(defn parse-body [body]
  (cheshire/parse-string (slurp body) true))

(defn cleanup-db [f]
  (swap! jobs (fn [jobs] []))
  (swap! agents (fn [agents] {}))
  (f))

(use-fixtures :each cleanup-db)

(deftest add-agent-test
  (testing "Test the add agent endpoint"
    (let [agent-1 {:id                 "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"
                   :name               "BoJack Horseman"
                   :primary_skillset   ["bills-questions"]
                   :secondary_skillset []}
          response (app (-> (mock/request :post "/api/agents/")
                            (mock/json-body agent-1)))
          body (parse-body (:body response))]
      (is (= (:status response) 200))
      (is (= body agent-1)))))

(deftest add-job-test
  (testing "Test the add job endpoint"
    (let [job-1 {:id     "f26e890b-df8e-422e-a39c-7762aa0bac36"
                 :type   "rewards_question"
                 :urgent false}
          response (app (-> (mock/request :post "/api/jobs/")
                            (mock/json-body job-1)))
          body (parse-body (:body response))]
      (is (= (:status response) 200))
      (is (= body job-1)))))

(deftest request-job-test-404-no-suitable-job
  (testing "Test the job request endpoint - no matching job."
    (add-agent {:id                 "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"
                :name               "BoJack Horseman"
                :primary_skillset   ["bills-questions"]
                :secondary_skillset []})
    (let [job-request-1 {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"}
          response (app (-> (mock/request :post "/api/job-requests/")
                            (mock/json-body job-request-1)))]
      (is (= (:status response) 404)))))

(deftest request-job-test-404-no-user
  (testing "Test the job request endpoint - no matching user id."
    (let [job-request-1 {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"}
          response (app (-> (mock/request :post "/api/job-requests/")
                            (mock/json-body job-request-1)))]
      (is (= (:status response) 404)))))

(deftest request-job-ordering-urgent-test
  (testing "Test the job request endpoint - matching job, pick urgent first"
    (add-agent {:id                 "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"
                :name               "BoJack Horseman"
                :primary_skillset   ["bills-questions"]
                :secondary_skillset []})
    (add-job {:id "69129bd0-409f-42d7-a4b2-ea09be8cc3e1" :type "rewards-questions" :urgent false})
    (add-job {:id "6734a9fc-306a-432c-8a62-9296d14f0019" :type "bills-questions" :urgent false})
    (add-job {:id "a81f045d-f720-436d-88bc-ef5abe7f7f68" :type "bills-questions" :urgent true})
    (let [job-request-1 {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"}
          response (app (-> (mock/request :post "/api/job-requests/")
                            (mock/json-body job-request-1)))
          body (parse-body (:body response))]
      (is (= (:status response) 200))
      (is (= (body :job_id) "a81f045d-f720-436d-88bc-ef5abe7f7f68")))))

(deftest request-job-ordering-primary-skill-test
  (testing "Test the job request endpoint - pick primary skill first"
    (add-agent {:id                 "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"
                :name               "BoJack Horseman"
                :primary_skillset   ["rewards-questions"]
                :secondary_skillset ["bills-questions"]})
    (add-job {:id "a81f045d-f720-436d-88bc-ef5abe7f7f68" :type "other-questions" :urgent false})
    (add-job {:id "6734a9fc-306a-432c-8a62-9296d14f0019" :type "bills-questions" :urgent true})
    (add-job {:id "69129bd0-409f-42d7-a4b2-ea09be8cc3e1" :type "rewards-questions" :urgent false})
    (let [job-request-1 {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"}
          response (app (-> (mock/request :post "/api/job-requests/")
                            (mock/json-body job-request-1)))
          body (parse-body (:body response))]
      (is (= (:status response) 200))
      (is (= (body :job_id) "69129bd0-409f-42d7-a4b2-ea09be8cc3e1")))))

(deftest request-job-secondary-skill-test
  (testing "Test the job request endpoint - matching job, secondary skill"
    (add-agent {:id                 "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"
                :name               "BoJack Horseman"
                :primary_skillset   ["rewards-questions"]
                :secondary_skillset ["bills-questions"]})
    (add-job {:id "69129bd0-409f-42d7-a4b2-ea09be8cc3e1" :type "bills-questions" :urgent false})
    (add-job {:id "a81f045d-f720-436d-88bc-ef5abe7f7f68" :type "bills-questions" :urgent false})
    (add-job {:id "6734a9fc-306a-432c-8a62-9296d14f0019" :type "bills-questions" :urgent true})
    (let [job-request-1 {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f6260"}
          response (app (-> (mock/request :post "/api/job-requests/")
                            (mock/json-body job-request-1)))
          body (parse-body (:body response))]
      (is (= (:status response) 200))
      (is (= (body :job_id) "6734a9fc-306a-432c-8a62-9296d14f0019")))))
